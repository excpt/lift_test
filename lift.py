import asyncio


from liftio import IO


class Lift():
    _calls_queue = asyncio.PriorityQueue()

    def __init__(self, floors_count=15, opened_doors_time=1, transition_time=1):
        assert 5 <= floors_count <= 20
        self._opened_doors_time = opened_doors_time
        self._transition_time = transition_time
        self._current_floor = 1
        self._goal_floor = 1
        self._floors_count = floors_count
        self._is_running = False

    @asyncio.coroutine
    def append_call(self, floor_num, priority):
        if 1 <= floor_num <= self._floors_count:
            yield from self._calls_queue.put((priority, floor_num))

    @asyncio.coroutine
    def open_doors(self):
        yield from IO.write('opening the doors\n')

    @asyncio.coroutine
    def close_doors(self):
        yield from IO.write('closing the doors\n')

    @asyncio.coroutine
    def wait_with_opened_doors(self):
        yield from asyncio.sleep(self._opened_doors_time)

    @asyncio.coroutine
    def transition(self, inc):
        yield from asyncio.sleep(self._transition_time)
        self._current_floor += inc
        yield from IO.write('current floor is %d\n' % (self._current_floor))

    @asyncio.coroutine
    def open_and_close_doors(self):
        yield from self.open_doors()
        yield from self.wait_with_opened_doors()
        yield from self.close_doors()
        yield from self._remove_same_floor_calls(self._current_floor)

    @asyncio.coroutine
    def _remove_same_floor_calls(self, floor_num):
        items = filter(lambda x: x[1] == floor_num, self._calls_queue._queue)
        [self._calls_queue._queue.remove(i) for i in items]

    @asyncio.coroutine
    def move_to_floor(self, floor_num):
        self._goal_floor = floor_num
        floor_diff = floor_num - self._current_floor
        if floor_diff > 0:
            inc = 1
        elif floor_diff < 0:
            inc = -1
        else:
            yield from self.open_and_close_doors()
            return

        yield from IO.write('current floor is %d\n' % (self._current_floor))

        while True:
            yield from self.transition(inc)

            if self._current_floor == self._goal_floor:
                yield from self.open_and_close_doors()
                return

    @asyncio.coroutine
    def movement(self):
        yield from IO.write('Lift is running\n\n')
        self._is_running = True

        while self._is_running:
            floor_call = yield from  self._calls_queue.get()
            yield from self.move_to_floor(floor_call[1])
            yield from asyncio.sleep(0.001)

    def stop(self):
        self._is_running = False
        self._current_floor = 1
        self._goal_floor = 1
        self._calls_queue.queue = []


def input_handler_maker(lift):
    @asyncio.coroutine
    def _handler(input_data):
        if input_data.startswith('i'):
            priority = 1
        elif input_data.startswith('o'):
            priority = 2
        else:
            return
        try:
            floor_num = int(input_data[1:])
        except TypeError:
            return

        yield from lift.append_call(floor_num, priority)

    return _handler
