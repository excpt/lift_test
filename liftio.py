import asyncio
import os
import sys
from asyncio.streams import StreamWriter, FlowControlMixin

from utils import infinite


class IO:
    reader, writer = None, None
    input_cb = None

    @classmethod
    def install(cls):
        if (cls.reader, cls.writer) == (None, None):
            cls.reader, cls.writer = yield from get_stdio()

    @classmethod
    @asyncio.coroutine
    def write(cls, msg):
        cls.writer.write(msg.encode('utf8'))
        yield from cls.writer.drain()

    @classmethod
    def register_input_cb(cls, cb):
        cls.input_cb = cb

    @classmethod
    @infinite
    @asyncio.coroutine
    def listen(cls):
        input_data = (yield from cls.reader.readline()).decode('utf8')
        if '\r' in input_data or '\n' in input_data:
            if cls.input_cb:
                yield from cls.input_cb(input_data)


@asyncio.coroutine
def get_stdio():
    loop = asyncio.get_event_loop()

    reader = asyncio.StreamReader()
    reader_protocol = asyncio.StreamReaderProtocol(reader)

    writer_transport, writer_protocol = yield from loop.connect_write_pipe(
        FlowControlMixin, os.fdopen(1, 'wb'))
    writer = StreamWriter(writer_transport, writer_protocol, None, loop)

    yield from loop.connect_read_pipe(lambda: reader_protocol, sys.stdin)

    return reader, writer
