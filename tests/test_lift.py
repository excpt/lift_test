from unittest import TestCase
from unittest.mock import patch
import asyncio
from asyncio import coroutine

from lift import Lift
from liftio import IO


def run_sync(coro, loop=None):
    if not loop:
        loop = asyncio.get_event_loop()
    loop.run_until_complete(coro)


def fake_coro(return_value=None):
    return coroutine(lambda *x, **y: return_value)()


class LiftTest(TestCase):

    def setUp(self):
        self.lift = Lift()

    @patch.object(asyncio.PriorityQueue, 'put', return_value=fake_coro())
    def test_append_call(self, put_mock):
        run_sync(self.lift.append_call(1, 1))
        put_mock.assert_called_with((1, 1))
        put_mock.reset_mock()

        run_sync(self.lift.append_call(-1, 1))
        self.assertEqual(put_mock.called, False)
        put_mock.reset_mock()

        run_sync(self.lift.append_call(16, 1))
        self.assertEqual(put_mock.called, False)
        put_mock.reset_mock()

    @patch.object(asyncio.PriorityQueue, 'get', return_value=fake_coro((1, 1)))
    @patch.object(Lift, 'move_to_floor', return_value=fake_coro())
    @patch.object(IO, 'write', return_value=fake_coro())
    def test_movement(self, write_mock, move_to_floor_mock, queue_mock):
        next(iter(self.lift.movement()))
        self.assertEqual(queue_mock.call_count, 1)
        self.assertEqual(move_to_floor_mock.call_count, 1)
        move_to_floor_mock.assert_called_with(1)

    @patch.object(IO, 'write', return_value=fake_coro())
    def test_open_doors(self, write_mock):
        run_sync(self.lift.open_doors())
        self.assertEqual(write_mock.call_count, 1)

    @patch.object(IO, 'write', return_value=fake_coro())
    def test_close_doors(self, write_mock):
        run_sync(self.lift.close_doors())
        self.assertEqual(write_mock.call_count, 1)

    @patch('asyncio.sleep', return_value=fake_coro())
    def test_wait_with_opened_doors(self, sleep_mock):
        run_sync(self.lift.wait_with_opened_doors())
        self.assertEqual(sleep_mock.call_count, 1)
        sleep_mock.assert_called_once_with(self.lift._opened_doors_time)

    @patch('asyncio.sleep', return_value=fake_coro())
    @patch.object(IO, 'write', return_value=fake_coro())
    def test_transition_upside(self, write_mock, sleep_mock):
        run_sync(self.lift.transition(1))
        self.assertEqual(sleep_mock.call_count, 1)
        sleep_mock.assert_called_with(self.lift._transition_time)
        self.assertEqual(write_mock.call_count, 1)
        self.assertEqual(self.lift._current_floor, 2)

    @patch('asyncio.sleep', return_value=fake_coro())
    @patch.object(IO, 'write', return_value=fake_coro())
    def test_transition_downside(self, write_mock, sleep_mock):
        self.lift._current_floor = 2
        run_sync(self.lift.transition(-1))
        self.assertEqual(sleep_mock.call_count, 1)
        sleep_mock.assert_called_with(self.lift._transition_time)
        self.assertEqual(write_mock.call_count, 1)
        self.assertEqual(self.lift._current_floor, 1)

    @patch.object(Lift, 'transition', return_value=fake_coro())
    @patch.object(Lift, 'append_call', return_value=fake_coro())
    @patch.object(IO, 'write', return_value=fake_coro())
    @patch.object(Lift, 'open_and_close_doors', return_value=fake_coro())
    def test_move_to_floor_no_transition(self, open_and_close_doors_mock, write_mock,
                                         *mocks):
        run_sync(self.lift.move_to_floor(1))
        self.assertEqual(open_and_close_doors_mock.call_count, 1)
        self.assertEqual(self.lift._current_floor, 1)
        [self.assertEqual(m.called, False) for m in mocks]

    @patch.object(Lift, 'append_call', return_value=fake_coro())
    @patch.object(IO, 'write', return_value=fake_coro())
    @patch.object(Lift, 'open_and_close_doors', return_value=fake_coro())
    def test_move_to_floor_with_transition(self, open_and_close_doors_mock,
                                               write_mock, append_call_mock, ):
        run_sync(self.lift.move_to_floor(2))
        self.assertEqual(open_and_close_doors_mock.call_count, 1)
        self.assertEqual(self.lift._current_floor, 2)
        self.assertEqual(write_mock.call_count, 2)
        self.assertEqual(append_call_mock.call_count, 0)
        self.assertEqual(open_and_close_doors_mock.call_count, 1)
