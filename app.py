import argparse
import asyncio

from lift import Lift, input_handler_maker
from liftio import IO


@asyncio.coroutine
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("height")
    parser.add_argument("speed")
    parser.add_argument("floors_count")
    parser.add_argument("doors_time")
    args = parser.parse_args()

    lift = Lift(
        floors_count=int(args.floors_count),
        opened_doors_time=float(args.doors_time),
        transition_time=float(args.height) / float(args.speed),
    )
    IO.register_input_cb(input_handler_maker(lift))

    yield from IO.install()

    yield from asyncio.gather(IO.listen(), lift.movement())


if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())
