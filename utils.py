import asyncio


def infinite(f):
    @asyncio.coroutine
    def wrapped(*args):
        while True:
            yield from f(*args)

    return wrapped
